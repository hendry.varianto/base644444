import "./App.css";
import { useState } from "react";

function App() {
  const [image, setImage] = useState("");

  return (
    <div className="App" style={{ width: "100%" }}>
      <header className="App-header">
        <p>Enter Image Here</p>
        <input
          type="file"
          onChange={(e) => {
            // console.log(URL.createObjectURL(e.target.files[0]));
            if (e.target.files) {
              var reader = new FileReader();
              reader.readAsDataURL(e.target.files[0]);
              reader.onload = function () {
                setImage(reader.result);
              };
              reader.onerror = function (error) {
                console.log("Error: ", error);
              };
            }
          }}
          style={{ marginBottom: "50px" }}
        />
        <textarea style={{ width: "1000px", height: "600px" }} value={image} />
      </header>
    </div>
  );
}

export default App;
